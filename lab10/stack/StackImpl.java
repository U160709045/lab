package stack;

public class StackImpl<T> implements Stack<T> {
	StackItem<T> top;
	
	@Override
	public void push(T item) {
		StackItem<T> newTop = new StackItem<>(item, top);
		top = newTop;
	}
	
	public T pop() {
		T item = top.getItem();
		
		top = top.getPrevious();
		return item;
		
	}
	
	@Override
	public boolean empty() {
		return top = null;
	}

}
