package quiz2;
import java.util.*;

public class Quest6 {
	int value;

	public static void main(String[] args) {
		Map<Quest6, String> map = new LinkedHashMap<>();
		map.put(new Quest6(3), "a");
		map.put(new Quest6(7), "ab");
		map.put(new Quest6(8), "abc");
		map.put(new Quest6(2), "a");
		map.put(new Quest6(5), "bc");
		System.out.println(map);
	}
	

	public Quest6(int value) {
		this.value = value;
	}
	@Override
	public boolean equals(Object o) {
		if (o instanceof Quest6){
			return value % 5 == ((Quest6)o).value % 5;
		}
		return false;
	}
	

	public String toString() {
		return "" + value;
	}
	

	public int hashCode() {
		return 0;
	}
}
