package quiz2;
import java.util.*;

public class Quest3 implements Comparable<Quest3> {
	String value;

	public static void main(String[] args) {
		Set<Quest3> set = new TreeSet<>();
		set.add(new Quest3("abc"));
		set.add(new Quest3("a"));
		set.add(new Quest3("bc"));
		set.add(new Quest3("ab"));
		set.add(new Quest3("c"));
		System.out.println(set);
	}
	

	public Quest3(String value){
		this.value = value;
	}
	

	public int compareTo(Quest3 q) {
		return q.value.length() - value.length();
	}

	public String toString() {
		return value;
	}
}
