package quiz2;
import java.util.*;

public class Quest4 {
	int value;

	public static void main(String[] args) {
		Set<Quest4> set = new HashSet<>();
		Quest4 q = new Quest4(7);
		System.out.println(q.hashCode());
		set.add(q);
		set.add(new Quest4(5));
		set.add(new Quest4(3));
		set.add(new Quest4(0));
		set.add(new Quest4(2));
		
		
		System.out.println(set);
	}

	public Quest4(int value){
		this.value = value;
	}
	

	public boolean equals(Object o){
		if (o instanceof Quest4){
			return value % 5 == ((Quest4)o).value % 5;
		}
		return false;
	}
	public String toString() {
		return "" + value;
	}
	@Override
	public int hashCode() {
		return 0;
	
	}
}