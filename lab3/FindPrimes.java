public class FindPrimes {

    public static void main(String args[]) {

        int n = Integer.parseInt(args[0]);
		int t1 = 0;
		int t2 = 1;
        
        System.out.print("Upto Argument Number as a Fibonacci Sequence is " + n + ": ");
        while (t1 <= n)
        {
            System.out.print(t1 + " , ");

            int sum = t1 + t2;
            t1 = t2;
            t2 = sum;
        }
    }
}