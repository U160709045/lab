package exam;

public class Question2 {
	
	private static int min(int c, int b, int a) {
		
		int min = a < b ? a : b;
		return min < c ? min : c;
			
	}
	

	public static void main(String[] args) {
		System.out.println(min(6, 8, 10));

	}

}
