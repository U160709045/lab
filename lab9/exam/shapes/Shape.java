package exam.shapes;

public abstract class Shape {
	
	public abstract double area();
}
