public class GCDLoop {
	
	
	public static int gcd(int a, int b) {
        while(a!=0 && b!=0) {
            int c = b;
            b = a%b;
            a = c;
        }
        return a+b; 
    }
	
	public static void main(String args[]) {
        
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        int d  = gcd(a, b);
        
        System.out.println("gcd(" + a + ", " + b + ") = " + d);
        
	
    }

}